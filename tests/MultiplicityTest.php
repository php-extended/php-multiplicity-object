<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-multiplicity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Multiplicity\Multiplicity;
use PHPUnit\Framework\TestCase;

/**
 * MultiplicityTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Multiplicity\Multiplicity
 *
 * @internal
 *
 * @small
 */
class MultiplicityTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Multiplicity
	 */
	protected Multiplicity $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
