<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-multiplicity-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Multiplicity;

use ReflectionException;

/**
 * Multiplicity class file.
 * 
 * This class represents multiplicities for a relational model.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.StaticAccess")
 */
enum Multiplicity : string implements MultiplicityInterface
{
	
	
	/**
	 * Finds the multiplicity with the given mandatory and multiple values.
	 * 
	 * @param boolean $mandatory
	 * @param boolean $multiple
	 * @return MultiplicityInterface
	 * @throws ReflectionException
	 */
	public static function findBy(bool $mandatory, bool $multiple) : MultiplicityInterface
	{
		foreach(Multiplicity::cases() as $multiplicity)
		{
			/** @var Multiplicity $multiplicity */
			if($multiplicity->isMandatory() === $mandatory && $multiplicity->isMultiple() === $multiple)
			{
				return $multiplicity;
			}
		}
		
		return Multiplicity::ZERO_MANY;
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Multiplicity\MultiplicityInterface::equals()
	 */
	public function equals($object) : bool
	{
		return $this === $object;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Multiplicity\MultiplicityInterface::isMandatory()
	 */
	public function isMandatory() : bool
	{
		return \strpos($this->value, '1') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Multiplicity\MultiplicityInterface::isMultiple()
	 */
	public function isMultiple() : bool
	{
		return \strpos($this->value, 'n') !== false;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Multiplicity\MultiplicityInterface::mergeWith()
	 * @throws ReflectionException
	 */
	public function mergeWith(MultiplicityInterface $other) : MultiplicityInterface
	{
		return static::findBy(
			$this->isMandatory() && $other->isMandatory(),
			$this->isMultiple() || $other->isMultiple(),
		);
	}

	case ZERO_ONE = '0-1';
	case ZERO_MANY = '0-n';
	case ONE_ONE = '1-1';
	case ONE_MANY = '1-n';
	
}
